/*
 * Copyright (C) 2015 hcadavid
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.eci.pdsw.math.fractions.testing;

import edu.eci.pdsw.math.crappyfractions.Fraction;
import edu.eci.pdsw.math.crappyfractions.InvalidFractionException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * Clases de equivalencia:

    CLASE 1: DESCRIPCION.
    TIPO: (NORMAL, DE FRONTERA)
    SE ESPERA UN ERROR: SI() NO()



 */
public class FractionTest {
    
    public FractionTest() {
    }
    
    @Before
    public void setUp() {
    }
 
    @Test
    public void testSuma() throws InvalidFractionException{
        Fraction f1=new Fraction(1,2);
        Fraction f2=new Fraction(1,2);
        Fraction res=f1.add(f2);
        
        assertEquals("La suma de las fracciones 1/2 + 1/2 no se calcula correctamemte,"
                + "o no está normalizada.",res,new Fraction(1,1));
        
    }
    
    
}
