/*
 * Copyright (C) 2015 hcadavid
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.eci.pdsw.math.fractions;

import edu.eci.pdsw.math.crappyfractions.Fraction;
import edu.eci.pdsw.math.crappyfractions.InvalidFractionException;
import java.util.Scanner;

/**
 *
 * @author hcadavid
 */
public class ExampleOfUse {
 
    // For testing:
    public static void main(String[] args) throws InvalidFractionException {
        

        
        Fraction f1 = new Fraction(1, 2);
        System.out.println("f1: " + f1);

        Fraction f2 = new Fraction(1, 2);
        System.out.println("f2: " + f2);

        Fraction f3;

        // 1/2 + 1/2
        f3 = f1.add(f2);
        System.out.println("f1 + f2: " + f3);

        
        // 1/2 - 1/2
        f3 = f1.sub(f2);
        System.out.println("f1 - f2: " + f3);


        //Evaluar 1/2 == 1/2
        if (f1.equals(f2)) {
            System.out.println("f3 equals f1");
        }

    }
    
    
}
