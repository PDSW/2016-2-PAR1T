### Escuela Colombiana de Ingeniería
### PDSW – Procesos de desarrollo de Software
## Parcial Primer Tercio

Se permite: uso de código propio, apuntes, fotocopias, documentos
pdf.

**Implican anulación:** uso del celular, tener abierto un cliente de
correo o de redes sociales.

# Parte teórica

## (20%) Parte 0 - teórico (moodle)

# Parte práctica

Clone el proyecto mediante GIT (NO LO DESCARGUE COMO ZIP, PUES ESTO AFECTARÁ LOS PASOS DE ENTREGA!)

```java
git clone https://gitlab.com/PDSW/2016-2-PAR1T.git
```

## (40%) Parte I

1.  El proyecto disponible en la carpeta "Parte1"
    es una sencilla herramienta de visualización de información de datos
    de empleados (que corresponde al diagrama de abajo), la cual muestra
    información como nombre, tarifa por hora, y si trabaja en tiempo
    parcial o no (Part Time) a partir de una fuente de datos. Usted
    debe hacer un arreglo ‘rápido’, que haga que sólo se visualicen (de
    la fuente de datos utilizada EmployeeDataSourceStub), aquellos
    empleados que trabajan tiempo completo (es decir, donde *part time*
    es falso). Tenga en cuenta que usted sólo tiene capacidad de
    modificar el programa principal (MainProgram).

	![](./img/media/image2.png)

    a.  [10%, en el archivo RESPUESTAS.txt] Indique qué patrón de diseño
        sería aplicable para este caso.

    b.  [30%, en el proyecto] Resuelva el problema aplicando dicho patrón. 


## (40%) Parte II

1.  En una librería de utilidades está disponible la clase Fraction, la
    cual permite representar una fracción y realizar operaciones entre
    éstas (por ahora sumar y restar). Las siguientes son las
    especificaciones para el constructor y el método ‘add’ (el método
    subs –restar- tiene una especificación equivalente):

	```java
    /**
     * Inicializa una fracción con el numerador y denominador indicados
     * @param num el numerador de la fracción
     * @param den el denominicador de la fracción
     * @throws InvalidFractionException si se usa 0 como denominador
     */
    public Fraction(int num, int den) throws InvalidFractionException 


    /**
     * @obj calcular la suma de la fracción actual con una fracción dada, 
     * y retornar una nueva fracción SIMPLIFICADA que corresponda a dicha
     * suma.
     * @param f La fracción a sumar
     * @return una nueva fracción que corresponde a la suma de la fracción
     * actual (this) con la fracción recibida.
     */
    public Fraction add(Fraction f) 
	```



	Con lo anterior, si se quisiera calcular una fracción equivalente a la suma de las fracciones ¼ + ¼, bastaría con hacer:

	```java
Fraction f1=new Fraction(1,4); 
Fraction f2=new Fraction(1,4);
Fraction f3=f1.add(f2); 
//f3 debería equivaler a la fracción 1/2
	```
	Un aspecto importante de esta clase (de acuerdo con la especificación),
es que garantiza que siempre se arrojarán fracciones simplificadas. Por
ejemplo, si se crea la fracción 10/20 y se le suma otra fracción 10/20,
el resultado siempre debe ser 1/1, nunca otras representaciones como
20/20 o 400/400.

	En la carpeta "Parte2" se tiene un programa (ExampleOfUse.java) que hace uso de la clase
Fraction en mención (de la cual no están disponibles los fuentes). Se
sabe que la clase Fraction tiene un defecto, pero como las pruebas con
las cuales se desarrolló la clase (clase FractionTest) son muy pobres,
nadie lo ha notado.

	a.  [10% En la clase de pruebas (FractionTest)] A partir de lo indicado de la clase
    Fraction, defina un conjunto de clases de equivalencia que cree que
    valgan la pena considerarse para la verificación del correcto
    funcionamiento de la clase. Para cada clase de equivalencia indique su tipo: Clase de equivalencia normal/Clase de equivalencia de análisis de frontera, y si los resultados esperados para dicha clase de equivalencia son errores o no. 

	b.  [20% En el proyecto] Implemente pruebas para dichas clases de
    equivalencia (indique en el código a que clase corresponde
    cada prueba). Haga, a lo sumo, cinco casos de prueba (los que considere más relevantes).

	c.  [10% En el archivo RESPUESTAS.txt] Basado en los resultados de las
    pruebas indique: cual es el defecto que tiene la clase Fraction?

	Recuerde que para ejecutar las pruebas, puede invocar la fase ‘test’ de
Maven: mvn test

## Entrega

Siga al pie de la letra estas indicaciones para la entrega del examen.

1. Limpie ambos proyectos, ejecutando este comando desde las carpetas Punto1 y Punto2:

	```bash
$ mvn clean
```

1. Configure su usuario de GIT

	```bash
$ git config --global user.name "Juan Perez"
$ git config --global user.email juan.perez@escuelaing.edu.co
```

2. Desde el directorio raíz (donde está este archivo README.md), haga commit de lo realizado.

	```bash
$ git add .
$ git commit -m "entrega parcial 1 - Juan Perez"
```


3. Desde este mismo directorio, comprima todo con: (no olvide el punto al final en la segunda instrucción)

	```bash
$ zip -r APELLIDO.NOMBRE.zip .
```
4. Abra el archivo ZIP creado, y rectifique que contenga lo desarrollado.

4. Suba el archivo antes creado (APELLIDO.NOMBRE.zip) en el espacio de moodle correspondiente.

5. IMPORTANTE!. Conserve una copia de la carpeta y del archivo .ZIP.