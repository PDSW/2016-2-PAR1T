/*
 * Copyright (C) 2015 hcadavid
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.eci.pdsw.webappsintro.pdsw.parcial1t;

import edu.eci.pdsw.stubs.datasourcestub.EmployeeDataSourceStub;
import edu.eci.pdsw.toolstubs.dataviewer.Employee;
import edu.eci.pdsw.toolstubs.dataviewer.EmployeesViewer;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.swing.SwingUtilities;

/**
 *
 * @author hcadavid
 */
public class MainProgram {
    
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                
                Set<Employee> se=new LinkedHashSet<>();
                se.add(new Employee(11,"xx",121,true));
                se.add(new Employee(22,"yy",121,true));
                
                EmployeeDataSourceStub ds=new EmployeeDataSourceStub();                
                new EmployeesViewer(ds);
            }
        });
    }   
    
    
}
